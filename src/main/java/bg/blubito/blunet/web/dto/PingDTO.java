package bg.blubito.blunet.web.dto;

import java.time.Instant;

public class PingDTO {

    private Instant serverTime;
    private String version;

    public PingDTO() {
        this.serverTime = Instant.now();
    }

    public Instant getServerTime() {
        return serverTime;
    }

    public void setServerTime(Instant serverTime) {
        this.serverTime = serverTime;
    }

    public String getVersion() {
        return version;
    }
}
