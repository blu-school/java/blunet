package bg.blubito.blunet.web;

import bg.blubito.blunet.service.AppUserService;
import bg.blubito.blunet.web.dto.AppUserDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppUserResource {

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/users")
    @ApiOperation(value = "Returns list of all users")
    public ResponseEntity<List<AppUserDTO>> getUsers() {
        return ResponseEntity.ok(appUserService.getUsers());
    }
}
