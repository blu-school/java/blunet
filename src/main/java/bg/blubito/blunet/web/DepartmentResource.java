package bg.blubito.blunet.web;

import bg.blubito.blunet.service.DepartmentService;
import bg.blubito.blunet.web.dto.DepartmentDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class DepartmentResource {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/departments")
    @ApiOperation(value = "Return list of all departments")
    public  ResponseEntity<List<DepartmentDTO>> getDepartments() {
        return ResponseEntity.ok(departmentService.getDepartments());
    }

    @GetMapping("/departments/{id}")
    @ApiOperation(value = "Returns a department by given id")
    public  ResponseEntity<DepartmentDTO> getDepartment(@PathVariable Long id) {
        return ResponseEntity.ok(departmentService.getDepartment(id));
    }

    @PostMapping("/departments")
    @ApiOperation(value = "Create new department")
    public ResponseEntity<DepartmentDTO> createDepartment(@Valid @RequestBody DepartmentDTO departmentDTO) throws URISyntaxException {
        DepartmentDTO result = departmentService.createDepartment(departmentDTO);
        return ResponseEntity.created(new URI("/api/departments/" + result.getId())).body(result);
    }

    @PutMapping("/departments/{id}")
    @ApiOperation(value = "Update department by given id")
    public ResponseEntity<DepartmentDTO> updateDepartment(@Valid @RequestBody DepartmentDTO departmentDTO, @PathVariable Long id) {
        return ResponseEntity.ok(departmentService.updateDepartment(departmentDTO, id));
    }

    @DeleteMapping("/departments/{id}")
    @ApiOperation(value = "Delete department by given id")
    public void deleteDepartment(@PathVariable Long id) {
        departmentService.deleteDepartment(id);
    }


}
