package bg.blubito.blunet.web;

import bg.blubito.blunet.web.dto.PingDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PingResource {

    @GetMapping("/ping")
    @ApiOperation(value = "ping", notes = "This resource returns current time in UTC format.")
    public ResponseEntity<PingDTO> ping() {
        return new ResponseEntity<>(new PingDTO(), HttpStatus.OK);
    }
}
