package bg.blubito.blunet.service;

import bg.blubito.blunet.entity.Department;
import bg.blubito.blunet.exception.ApiRequestException;
import bg.blubito.blunet.exception.EntryNotFoundException;
import bg.blubito.blunet.repository.DepartmentRepository;
import bg.blubito.blunet.web.dto.DepartmentDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<DepartmentDTO> getDepartments() {
        List<Department> departments = departmentRepository.findAll();
        List<DepartmentDTO> departmentDTOs = new ArrayList<>();
        for (Department department : departments) {
            departmentDTOs.add(modelMapper.map(department, DepartmentDTO.class));
        }
        return departmentDTOs;
    }

    public DepartmentDTO getDepartment(Long id) {
        Optional<Department> department = departmentRepository.findById(id);
        if (!department.isPresent()) throw new EntryNotFoundException("Can not find department with ID " + id);
        return modelMapper.map(department.get(), DepartmentDTO.class);
    }

    public DepartmentDTO createDepartment(DepartmentDTO departmentDTO) {
        if(departmentRepository.findByNameIgnoreCase(departmentDTO.getName()) != null)
            throw new ApiRequestException("Department name is already taken.");
        Department department = modelMapper.map(departmentDTO, Department.class);
        Department result = departmentRepository.save(department);
        return modelMapper.map(result, DepartmentDTO.class);
    }

    public DepartmentDTO updateDepartment(DepartmentDTO departmentDTO, Long id) {
        Optional<Department> department = departmentRepository.findById(id);
        if (!department.isPresent()) throw new EntryNotFoundException("Can not find department with ID " + id);
        if(departmentRepository.findByNameIgnoreCase(departmentDTO.getName()) != null)
            throw new ApiRequestException("Department name is already taken.");
        Department updatedDepartment = department.get();
        updatedDepartment.setName(departmentDTO.getName());
        Department result = departmentRepository.save(updatedDepartment);
        return modelMapper.map(result, DepartmentDTO.class);
    }

    public void deleteDepartment(Long id) {
        Optional<Department> department = departmentRepository.findById(id);
        if (!department.isPresent()) throw new EntryNotFoundException("Can not find department with ID " + id);
        departmentRepository.deleteById(id);
    }
}
