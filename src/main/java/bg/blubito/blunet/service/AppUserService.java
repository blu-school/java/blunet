package bg.blubito.blunet.service;

import bg.blubito.blunet.entity.AppUser;
import bg.blubito.blunet.repository.AppUserRepository;
import bg.blubito.blunet.web.dto.AppUserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<AppUserDTO> getUsers() {
        List<AppUser> appUsers = appUserRepository.findAll();
        List<AppUserDTO> appUserDTOs = new ArrayList<>();
        for(AppUser appUser : appUsers) {
            appUserDTOs.add(modelMapper.map(appUser, AppUserDTO.class));
        }
        return appUserDTOs;
    }
}
