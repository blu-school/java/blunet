package bg.blubito.blunet.repository;

import bg.blubito.blunet.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
}
